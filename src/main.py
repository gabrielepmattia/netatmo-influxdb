#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from auth import Auth
from config import Config
from database import Database
from env import Environment
from log import Log
from web import app
from worker import Worker

MODULE = "Main"

Log.minfo(MODULE, f"Starting application v{Config.get_app_version()}")

# retrieve vars
host = Environment.get_server_hostname()
port = Environment.get_server_port()

# checks
app_id = Environment.get_app_client_id()
app_secret = Environment.get_app_secret()

if app_id == "" or app_secret == "":
    print("No app secrets defined")
    exit(1)

# prepare singletons instances
Auth.instance()
Database.instance()

# start worker
Worker.instance().run()

Log.minfo(MODULE, f"Starting webserver at {host}:{port}")
# start web server
app.run(host=host, port=port)
