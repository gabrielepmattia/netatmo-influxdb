#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime

from flask import Flask, request

from api import API
from auth import Auth
from measurements import Measurements

app = Flask(__name__)


@app.route('/')
def root():
    out = "To (re)authorize the app go to <a href=\"/auth\">/auth</a>, to manually poll visit <a href=\"/poll\">/poll</a>."

    auth_obj = Auth.instance()
    if auth_obj.is_authorized():
        out += "\n\nApp is authorized."

    return out


@app.route('/auth')
def auth():
    href = Auth.get_oauth_url()
    return f"Click <a href=\"{href}\">here</a> to authorize the app"


@app.route('/auth/ok')
def auth_ok():
    state = request.args.get(Auth.OAUTH_KEY_STATE)
    code = request.args.get(Auth.OAUTH_KEY_CODE)

    # perform authorization
    auth = Auth.instance()
    res = auth.authorize(code)

    return f"Success: {res}<br />" \
           f"Access Token: <pre>{auth.get_access_token()}</pre><br />" \
           f"Refresh Token: <pre>{auth.get_refresh_token()}</pre><br />" \
           f"Please copy the authorization data to your container if in kiosk mode"


@app.route('/poll')
def poll():
    # retrieve data
    res_json = API.get_stations_data()
    # log the measurements to DB
    if res_json is not None:
        time = datetime.now()
        Measurements.log_measurements_from_response(res_json, time)
        return res_json
    return "No response. Check log."


if __name__ == '__main__':
    app.run()
