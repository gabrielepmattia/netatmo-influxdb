#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime

import dateutil
from rfc3339 import rfc3339

from database import Database
from log import Log

MODULE = "Measurements"


class Measurements:

    @staticmethod
    def log_measurements_from_response(res_json: dict, time):
        """Extract and log all data from Netatmo response"""
        # check devices object
        if res_json is None or "body" not in res_json.keys() or "devices" not in res_json["body"]:
            Log.mwarn(MODULE, f"log_measurements_from_response: cannot log data since bad device object: none={res_json is None}")
            return

        for device in res_json["body"]["devices"]:
            home_name = device["home_name"]
            for data_type in device["data_type"]:
                Measurements._log_measurement(device, time, home_name, data_type)
            for module in device["modules"]:
                for data_type in module["data_type"]:
                    Measurements._log_measurement(module, time, home_name, data_type)

        Log.mdebug(MODULE, f"get_station_data: date is {rfc3339(datetime.now())}")

    @staticmethod
    def _log_measurement(device, time, home_name, measurement_key):
        """Log a single measurement to DB"""
        # check device object
        if device is None or "dashboard_data" not in device.keys():
            Log.mwarn(MODULE, f"_log_measurement: cannot log data since bad device object: none={device is None}")
            return

        dashboard = device["dashboard_data"]
        db = Database.instance()
        db.add_measurement(
            device["_id"],
            home_name + "_" + device["module_name"],
            measurement_key,
            float(dashboard[measurement_key]),
            datetime.fromtimestamp(dashboard["time_utc"], tz=dateutil.tz.gettz('UTC'))
        )
