#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import time

import requests

from env import Environment
from files import Files
from log import Log
from models import AuthCredentials
from utils import Utils

MODULE = "Auth"


class Auth:
    """This is a singleton"""
    API_OAUTH_GRANT = "https://api.netatmo.com/oauth2/token"
    API_OAUTH_SCOPE = "read_station"

    OAUTH_KEY_STATE = "state"
    OAUTH_KEY_CODE = "code"
    OAUTH_ACCESS_TOKEN = "access_token"
    OAUTH_EXPIRES_IN = "expires_in"
    OAUTH_REFRESH_TOKEN = "refresh_token"

    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)

            # init
            cls._credentials = Files.get_auth()
            cls._authorized = cls._credentials is not None

            if cls._credentials is not None:
                Log.minfo(MODULE, "instance: Credential file loaded from data path")
                Log.mdebug(MODULE, f"instance: Credentials: {cls._credentials}")

            Log.minfo(MODULE, "instance: init successfully")
        return cls._instance

    #
    # Getters
    #

    def get_access_token(self) -> str:
        return self._credentials.get_access_token() if self._credentials is not None else ""

    def get_refresh_token(self) -> str:
        return self._credentials.get_refresh_token() if self._credentials is not None else ""

    def is_authorized(self):
        return self._credentials is not None and self._authorized

    #
    # Statics
    #

    @staticmethod
    def get_redirect_url():
        return f"http://{Environment.get_server_hostname()}:{Environment.get_server_port()}/auth/ok"

    @staticmethod
    def get_oauth_url():
        app_id = Environment.get_app_client_id()
        redirect_url = Auth.get_redirect_url()
        state = Utils.get_random_str(16)
        return f"https://api.netatmo.com/oauth2/authorize?client_id={app_id}" \
               f"&redirect_uri={redirect_url}" \
               f"&scope={Auth.API_OAUTH_SCOPE}" \
               f"&state={state}"

    #
    # Core
    #

    # noinspection PyAttributeOutsideInit,PyBroadException
    def authorize(self, code):
        payload = {
            "grant_type": "authorization_code",
            "client_id": Environment.get_app_client_id(),
            "client_secret": Environment.get_app_secret(),
            "code": code,
            "redirect_uri": Auth.get_redirect_url(),
            "scope": Auth.API_OAUTH_SCOPE
        }

        try:
            res = requests.post("https://api.netatmo.com/oauth2/token", data=payload)
            json = res.json()
            if res.status_code == 200:
                self._save_credentials_from_json(json)
                return True

            Log.merr(MODULE, f"authorize: could not authorize token: status={res.status_code}")
            return False
        except Exception as e:
            Log.merr(MODULE, f"authorize: post error: {e}")
            return False

    def check_renew(self):
        """Check if code is to be renewed and renews it if needed"""
        Log.mdebug(MODULE, f"check_renew: called with self._credentials={self._credentials is not None}")
        if self._credentials.get_generated_at() + self._credentials.get_expires_in() - 100 > time.time():
            Log.mdebug(MODULE, f"check_renew: credentials do not need to be refreshed")
            return True

        payload = {
            "grant_type": "refresh_token",
            "refresh_token": self._credentials.get_refresh_token(),
            "client_secret": Environment.get_app_secret(),
            "client_id": Environment.get_app_client_id(),
        }

        try:
            res = requests.post("https://api.netatmo.com/oauth2/token", data=payload)
            json = res.json()
            if res.status_code == 200:
                Log.mdebug(MODULE, f"check_renew: post returned status={res.status_code}")
                self._save_credentials_from_json(json)
                return True

            Log.merr(MODULE, f"check_renew: could not refresh token: status={res.status_code}")
            return False

        except Exception as e:
            Log.merr(MODULE, f"check_renew: post error: {e}")
            return False

    #
    # Utils
    #

    # noinspection PyAttributeOutsideInit
    def _save_credentials_from_json(self, json):
        self._credentials = AuthCredentials(
            json[Auth.OAUTH_ACCESS_TOKEN],
            json[Auth.OAUTH_REFRESH_TOKEN],
            int(json[Auth.OAUTH_EXPIRES_IN]),
            time.time()
        )
        # save to file
        Files.save_auth(self._credentials)

        self._authorized = True
