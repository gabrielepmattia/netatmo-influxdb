#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import envparse


class Environment:
    ENV_APP_CLIENT_ID = "APP_CLIENT_ID"
    ENV_APP_SECRET = "APP_SECRET"

    ENV_POLLING_INTERVAL = "POLLING_INTERVAL"

    ENV_SERVER_PORT = "SERVER_PORT"
    ENV_SERVER_HOSTNAME = "SERVER_HOSTNAME"

    ENV_INFLUXDB_HOST = "INFLUXDB_HOST"
    ENV_INFLUXDB_PORT = "INFLUXDB_PORT"
    ENV_INFLUXDB_USERNAME = "INFLUXDB_USERNAME"
    ENV_INFLUXDB_PASSWORD = "INFLUXDB_PASSWORD"
    ENV_INFLUXDB_NAME = "INFLUX_DBNAME"

    @staticmethod
    def get_app_client_id() -> str:
        return envparse.env(Environment.ENV_APP_CLIENT_ID, default="")

    @staticmethod
    def get_app_secret() -> str:
        return envparse.env(Environment.ENV_APP_SECRET, default="")

    @staticmethod
    def get_server_hostname() -> str:
        return envparse.env(Environment.ENV_SERVER_HOSTNAME, default="0.0.0.0")

    @staticmethod
    def get_server_port() -> int:
        return envparse.env(Environment.ENV_SERVER_PORT, default=8080)

    @staticmethod
    def get_influxdb_host() -> str:
        return envparse.env(Environment.ENV_INFLUXDB_HOST, default="localhost")

    @staticmethod
    def get_influxdb_port() -> int:
        return envparse.env(Environment.ENV_INFLUXDB_PORT, default=8086)

    @staticmethod
    def get_influxdb_username() -> str:
        return envparse.env(Environment.ENV_INFLUXDB_USERNAME, default="")

    @staticmethod
    def get_influxdb_password() -> str:
        return envparse.env(Environment.ENV_INFLUXDB_PASSWORD, default="")

    @staticmethod
    def get_influxdb_name() -> str:
        return envparse.env(Environment.ENV_INFLUXDB_NAME, default="netatmo")

    @staticmethod
    def get_polling_interval() -> int:
        return envparse.env(Environment.ENV_POLLING_INTERVAL, default=300)
