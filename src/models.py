#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json


class AuthCredentials:
    def __init__(self, access_token, refresh_token, expires_in, generated_at: float):
        self._access_token = access_token
        self._refresh_token = refresh_token
        self._expires_in = expires_in
        self._generated_at = generated_at

    def get_access_token(self):
        return self._access_token

    def get_refresh_token(self):
        return self._refresh_token

    def get_expires_in(self):
        return self._expires_in

    def get_generated_at(self):
        return self._generated_at

    @staticmethod
    def from_dict(d):
        return AuthCredentials(d["access_token"], d["refresh_token"], d["expires_in"], d["generated_at"])

    def to_json(self) -> str:
        d = {
            "access_token": self._access_token,
            "refresh_token": self._refresh_token,
            "expires_in": self._expires_in,
            "generated_at": self._generated_at
        }
        return json.dumps(d, indent=2)


class Device:
    def __init__(self, json_dict):
        self._uid = json_dict["_id"]

        self._modules = []
        for module in json_dict["modules"]:
            self._modules.append(Module(module))


class Module:
    def __init__(self, json_dict):
        self._uid = json_dict["_id"]
        self._type = json_dict["type"]
