#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import time
from datetime import datetime
from threading import Thread

from api import API
from env import Environment
from log import Log
from measurements import Measurements

MODULE = "Worker"


class Worker:
    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
            # init
            Log.minfo(MODULE, "instance: init successfully")
        return cls._instance

    @staticmethod
    def _run():
        sleep_time = Environment.get_polling_interval()
        while True:
            Log.mdebug(MODULE, "_run: started polling")

            # retrieve data
            res_json = API.get_stations_data()
            # log the measurements to DB
            if res_json is not None:
                poll_time = datetime.now()
                Measurements.log_measurements_from_response(res_json, poll_time)

            Log.mdebug(MODULE, "_run: sleeping")
            time.sleep(sleep_time)

    def run(self, synchronous=False):
        if synchronous:
            self._run()
        else:
            thread = Thread(target=self._run)
            thread.start()
