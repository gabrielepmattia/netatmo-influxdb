#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import requests

from auth import Auth
from log import Log

MODULE = "Api"


class API:
    """Netatmo API"""

    BASE_URL = "https://api.netatmo.com/api"

    @staticmethod
    def get_authorization_headers():
        auth = Auth.instance()
        d = {
            "Authorization": f"Bearer {auth.get_access_token()}"
        }
        return d

    @staticmethod
    def get_stations_data():
        Log.mdebug(MODULE, "get_stations_data: called")
        auth = Auth.instance()
        if not auth.is_authorized():
            Log.mwarn(MODULE, "get_stations_data: not authorized, please authorize first or put auth.json")
            return None

        # check if we need to renew the token
        res = Auth.instance().check_renew()
        if not res:
            Log.merr(MODULE, f"get_stations_data: check_renew failed, skipping data retrieval")
            return None

        # perform the request
        try:
            res = requests.get(f"{API.BASE_URL}/getstationsdata", headers=API.get_authorization_headers())
            if res.status_code is not 200:
                Log.merr(MODULE, f"get_stations_data: error while retrieving data, status={res.status_code}")
                return None
            return res.json()
        except Exception as e:
            Log.merr(MODULE, f"get_stations_data: error during get: {e}")
            return None
