#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import json
import os

from models import AuthCredentials


class Files:
    FILES_DIR = "./data"

    FILE_AUTH_NAME = "auth.json"

    @staticmethod
    def get_auth() -> AuthCredentials or None:
        """Retrieve the auth credentials from file"""
        file_path = f"{Files.FILES_DIR}/{Files.FILE_AUTH_NAME}"
        if not os.path.isfile(file_path):
            return None

        f = open(file_path, "r")
        content = f.read()
        f.close()

        return AuthCredentials.from_dict(json.loads(content))

    @staticmethod
    def save_auth(credentials: AuthCredentials):
        os.makedirs(Files.FILES_DIR, exist_ok=True)
        f = open(f"{Files.FILES_DIR}/{Files.FILE_AUTH_NAME}", "w")
        print(credentials.to_json(), file=f)
        f.close()
