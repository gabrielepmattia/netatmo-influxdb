#  Netatmo InfluxDB Logger
#  Copyright (C) 2021 Gabriele Proietti Mattia <pm.gabriele@outlook.com>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from datetime import datetime

from influxdb import InfluxDBClient
from rfc3339 import rfc3339

from env import Environment
from log import Log

MODULE = "Database"


class Database:
    _instance = None

    def __init__(self):
        raise RuntimeError('Call instance() instead')

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls.__new__(cls)

            # init
            cls._client = InfluxDBClient(
                host=Environment.get_influxdb_host(),
                port=Environment.get_influxdb_port(),
                username=Environment.get_influxdb_username(),
                password=Environment.get_influxdb_password()
            )
            # create database if does not exist
            db_name = Environment.get_influxdb_name()
            db_list = cls._client.get_list_database()
            db_exists = False

            for db in db_list:
                if db["name"] == db_name:
                    db_exists = True
                    # cls._client.drop_database(db_name)
                    Log.mdebug(MODULE, f"instance: db {db_name} exists")
                    break
            if not db_exists:
                cls._client.create_database(db_name)

            cls._client.switch_database(db_name)

            Log.minfo(MODULE, "instance: init successfully")
        return cls._instance

    def add_measurement_min_max(self, device_id, device_name, measurement_type, measurement_value, measurement_value_min, measurement_value_max, timestamp: datetime):
        points = [
            {
                "measurement": measurement_type,
                "tags": {
                    "deviceId": device_id,
                    "deviceName": device_name
                },
                "time": rfc3339(timestamp),
                "fields": {
                    "current": measurement_value,
                    "min": measurement_value_min,
                    "max": measurement_value_max
                }
            },
        ]
        res = self._client.write_points(points)
        if res is False:
            Log.merr(MODULE, "add_measurement_min_max: cannot add point to db")
        else:
            Log.mdebug(MODULE, "add_measurement_min_max: point added successfully")

    def add_measurement(self, device_id, device_name, measurement_type, measurement_value, timestamp):
        points = [
            {
                "measurement": measurement_type,
                "tags": {
                    "deviceId": device_id,
                    "deviceName": device_name
                },
                "time": rfc3339(timestamp),
                "fields": {
                    "current": measurement_value,
                }
            },
        ]
        res = self._client.write_points(points)
        if res is False:
            Log.merr(MODULE, "add_measurement: cannot add point to db")
        else:
            Log.mdebug(MODULE, "add_measurement: point added successfully")
