FROM python:3.8

COPY ./src /app
WORKDIR /app

RUN pip install -r requirements.txt

EXPOSE 8080
VOLUME ./data

# set the Flask production environment
ENV FLASK_ENV=production

ENTRYPOINT [ "python3" ]
CMD [ "main.py" ]