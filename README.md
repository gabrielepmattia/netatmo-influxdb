# Netatmo Weather InfluxDB Logger

This module, packaged as a Docker container, periodically logs all the weather stations data to InfluxDB.

Check the project in which it is used at [gitlab.com/gabrielepmattia/netatmo-pi-desk](https://gitlab.com/gabrielepmattia/netatmo-pi-desk)